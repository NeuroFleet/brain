#!/usr/bin/python

from python2use.shortcuts import *

class Manager(PersistentCommandline):
    PROG  = 'nebula'
    HELP  = 'Nebula : Self-managed cloud of computing backends.'

    TITLE = "Nebula : Backend management tool."
    CFG_PATH = '/hml/etc/shelter/hosting.yml'

    def init_args(self):
        #self.parser.add_argument('iface', help='Network interface to use.')

        self.parser.add_argument('-t', '--type', dest='type', type=str,
                            default='auto', help="Type of provisonner ( %s ).)" % ' | '.join([str(prv) for prv in Nucleon._reg['backend']]))

    def prepare(self):
        self.cfg = Nucleon.local.read_yaml(self.CFG_PATH)

        for parts in [
            ('supervisor', 'conf.d'),
            ('supervisor', 'logs'),
            ('monitor', 'rrd'),
            ('config',),
        ]:
            pth = self.rpath(*parts)

            if not Nucleon.local.exists(pth):
                Nucleon.local.shell('mkdir', '-p', pth)

        Nucleon.local.render('cluster/supervisor/daemon.conf', self.rpath('supervisor', 'daemon.cfg'),
            bpath = self.rpath('supervisor'),
            ports = dict(http=60103),
            incls = self.rpath('supervisor', 'conf.d', '*.conf'),
        )

        Nucleon.local.shell('rm', '-fR', self.rpath('supervisor', 'conf.d', '*'))

        for nrw in self.cfg['backends']:
            bkn = Nucleon.get_backend(nrw, **self.cfg['backends'][nrw])

            if bkn:
                bkn.check()

        if not reduce(operator.or_, [
            Nucleon.local.exists(self.rpath('supervisor', 'daemon.%s' % k))
            for k in ('sock', 'pid')
        ]):
            Nucleon.local.shell('supervisord', '-c', self.rpath('supervisor', 'daemon.cfg'))

    def terminate(self):
        self.cfg['backends'] = dict([(x.narrow, x.__soul__) for x in Nucleon.backends.values()])

        Nucleon.local.write_yaml(self.CFG_PATH, self.cfg)

    bpath   = property(lambda self: '/shl/var/lib/nebula')

    def rpath(self, *pth):
        return os.path.join(self.bpath, *pth)

    @property
    def context(self):
        return {
            'bpath':   self.bpath,
            'iface':   self,
            'args':    self.args,
            'runpath': lambda *path: self.rpath('run', *path),
        }

    def resolve(self, *keys):
        return sorted([
            vht
            for vht in Nucleon.backends.values()
            if len(keys)==0 or (vht.narrow in keys)
        ], key=lambda vht: vht.narrow)

        return resp

    def superctl(self, *args, **kwargs):
        return Nucleon.local.shell('supervisorctl', '-c', self.rpath('supervisor', 'daemon.cfg'), *args, **kwargs)

    def badge_backend(self, bkn, level=0):
        prefix = "\t" * level

        print prefix+"-> owner    : %s" % bkn.owner
        print prefix+"   name     : %s" % bkn.name
        print prefix+"   provider : %s" % bkn.provider
        print prefix+"   ports    : %s" % bkn.ports
        print prefix+"   config   : %s" % bkn.cfg_path
        print prefix+"   path     : %s" % bkn.ctl_path

    def do_list(self, *keys):
        i = 0

        for bkn in self.resolve(*keys):
            if self.args['verbose']:
                if i!=0:
                    print

                self.badge_backend(bkn, level=1)

                i += 1
            else:
                print ("\t-> %s : %s" % (bkn.narrow, bkn.endpoint))

    def do_status(self):
        self.superctl('status')

    def do_reload(self):
        for key in ('reread', 'update'):
            self.superctl(key)

        self.do_status()

    def do_provision(self, narrow, *params):
        cfg = {}

        cfg['owner'], cfg['name'] = narrow.split('=', 1)

        for prm in params:
            k,v = prm.split('=', 1)

            cfg[k] = v

        cfg['provider'] = self.args['type']

        bkn = Nucleon.get_backend(narrow, **cfg)

        bkn.check()

        self.superctl('start', bkn.narrow)

    def do_debug(self, key):
        qs = self.resolve(key)

        if len(qs):
            qs[0].run()

    def do_scale(self, key, *hosts):
        qs = self.resolve(key)

        if len(qs):
            for entry in hosts:
                pass # qs[0] * entry

    def do_up(self, *keys):
        for bkn in self.resolve(*keys):
            self.superctl('start', bkn.narrow)

    def do_tail(self, *keys):
        for bkn in self.resolve(*keys):
            if bkn.exists('server.log'):
                bkn.shell('tail', '-f', bkn.rpath('server.log'))
            else:
                self.superctl('tail', '-f', bkn.narrow)

    def do_down(self, *keys):
        for bkn in self.resolve(*keys):
            self.superctl('stop', bkn.narrow)

    def do_destroy(self, *keys):
        for bkn in self.resolve(*keys):
            pass # self.superctl('start', bkn.narrow)

if __name__=='__main__':
    mgr = Manager()

    mgr()
