#!/bin/bash

export UCHIKOMA_ROOT=~/devel/cognito/ghost
export UCHIKOMA_DNS=uchikoma.xyz
export UCHIKOMA_BOT=proto

export UCHIKOMA_NETLOC="://$UCHIKOMA_BOT@$UCHIKOMA_DNS"

#export UCHIKOMA_LINK="django"$UCHIKOMA_NETLOC"/uchikoma?ns=uchikoma"
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&app[]=.connect'
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&app[]=.contrib.agora'
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&app[]=.contrib.brands'
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&app[]=.contrib.cables'

#export UCHIKOMA_LINK="scrapy"$UCHIKOMA_NETLOC"/uchikoma?"
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&spider[]='
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&spider[]='
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&spider[]='

#export UCHIKOMA_LINK="crossbar"$UCHIKOMA_NETLOC"/uchikoma?ns=uchikoma"
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&wsgi=.container'
#export UCHIKOMA_LINK=$UCHIKOMA_LINK'&sock=.uplink'

#export UCHIKOMA_LINK="hubot"$UCHIKOMA_NETLOC"/irc?server=irc.freenode.net&pseudo=tayamino&rooms=ubuntu-ma"
#export UCHIKOMA_LINK="hubot"$UCHIKOMA_NETLOC"/hipchat?domain=&pseudo=&secret="
#export UCHIKOMA_LINK="hubot"$UCHIKOMA_NETLOC"/messenger?verify=&access="
#export UCHIKOMA_LINK="hubot"$UCHIKOMA_NETLOC"/slack?token="
#export UCHIKOMA_LINK="hubot"$UCHIKOMA_NETLOC"/telegram?token="

#export UCHIKOMA_LINK="airflow"$UCHIKOMA_NETLOC"/"
export UCHIKOMA_LINK="luigi"$UCHIKOMA_NETLOC"/"

export `$UCHIKOMA_ROOT/bin/urlize $UCHIKOMA_LINK`

if [[ -d $UCHIKOMA_ROOT/var/pyenv ]] ; then
    source $UCHIKOMA_ROOT/var/pyenv/bin/activate
fi

python boot/strap $*
