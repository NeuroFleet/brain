from uchikoma.connect.shortcuts import *

user = Identity.objects.get(username='shivha')

mapping = [
    (['uchikoma.xyz'], 'Oklawamina2016', ['conan','loki','max','musashi','proto','viki']),

    (['tayaa.me'], 'Tesla2501', ['amine','shivha']),

    (['it-issal.club'], 'Tesla2501', ['arduino','raspberry']),
    (['it-issal.club'], 'Tesla2501', ['arduino','raspberry']),

    ([
        'babel-it.xyz',
    ]+[
        '.'.join([basedn,tld])
        for basedn,terms in [
            ('it-issal',['info','link','mobi','pw','review']),
            ('inbijas',['xyz','space','pro','trade','top']),
        ]
        for tld in terms
    ], 'Tesla2501', ['site','contact','apply','sales','support']),
]

for domains,passwd,listing in mapping:
    for fqdn in domains:
        for alias in listing:
            box,st = MailBox.objects.get_or_create(
                owner    = user,
                username = '%s@%s' % (alias, fqdn),
            )
            
            box.password = passwd
            box.hostname = 'mail.pawnmail.com'
            box.port     = 993
            
            box.save()

#user.refresh()

