# Natural Language Toolkit: code_traverse

import re, nltk

def traverse(t):
    try:
        t.label()
    except AttributeError:
        print(t, end=" ")
    else:
        # Now we know that t.node is defined
        print('(', t.label(), end=" ")
        for child in t:
            traverse(child)
        print(')', end=" ")

t = nltk.Tree('(S (NP Alice) (VP chased (NP the rabbit)))')

traverse(t) # ( S ( NP Alice ) ( VP chased ( NP the rabbit ) ) )

cp = nltk.RegexpParser('CHUNK: {<V.*> <TO> <V.*>}')
brown = nltk.corpus.brown

for sent in brown.tagged_sents():
    tree = cp.parse(sent)

    for subtree in tree.subtrees():
        if subtree.label() == 'CHUNK': print(subtree)


