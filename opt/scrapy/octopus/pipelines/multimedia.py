# -*- coding: utf-8 -*-

import scrapy

from scrapy.contrib.pipeline.images import ImagesPipeline
from scrapy.exceptions import DropItem

#****************************************************************************

class IllustratorPipeline(ImagesPipeline):
    def get_media_requests(self, item, info):
        if hasattr(item, 'filter_image'):
            for link in item['media'].get('links', []):
                yield scrapy.Request(link)
    
    def item_completed(self, results, item, info):
        if hasattr(item, 'filter_image'):
            resp = [item.filter_image(*kv) for kv in results if ok]
            
            if not resp:
                raise DropItem("Item contains no images")
            
            item['media']['paths'] = resp
        
        return item

