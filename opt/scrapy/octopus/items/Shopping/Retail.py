# -*- coding: utf-8 -*-

from octopus.utils import *

#****************************************************************************************

class RetailCategory(NativeItem):
    site        = scrapy.Field()
    uid         = scrapy.Field()
    
    title       = scrapy.Field()
    products    = scrapy.Field(default=[])
    
    #**********************************************************************************************************************
    
    def cleanup_sub(self, entry):
        for key in ('products', 'product'):
            if ('/%s/' % key) in entry['uid']:
                entry['uid'] = entry['uid'].replace(('/%s/' % key), '')
        
        entry['uid'] = int(entry['uid'])
        
        return entry
    
    def cleanup(self):
        if '/category/' in self['uid']:
            self['uid'] = self['uid'].replace('/category/','')
        
        self['uid'] = int(self['uid'])
        
        #*****************************************************************************************************************
        
        self['products']   = [self.cleanup_sub(sub) for sub in self['products']]
        
        #*****************************************************************************************************************
        
        return self
    
    #######################################################################################################################
    
    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")
    
    #######################################################################################################################
    
    class Schema:
        collection = 'retail_category'
        unique_key = [
            ('site', pymongo.ASCENDING),
            ('uid',  pymongo.ASCENDING),
        ]

class RetailArticle(NativeItem):
    site        = scrapy.Field()
    uid         = scrapy.Field()
    specs       = scrapy.Field(default={})
    
    title       = scrapy.Field()
    description = scrapy.Field()
    categories  = scrapy.Field()
    
    shipping    = scrapy.Field(default={})
    stock       = scrapy.Field(default={})
    pricing     = scrapy.Field(default={})
    
    media       = scrapy.Field(default={})
    
    #**********************************************************************************************************************
    
    def filter_image(self, state, entry):
        return entry['path']
    
    #**********************************************************************************************************************
    
    def cleanup(self):
        raw = self['uid']
        
        self['uid'] = ''
        
        for c in raw:
            try:
                x = int(c)
                
                self['uid'] += c
            except:
                pass
        
        self['uid'] = int(self['uid'])
        
        #*****************************************************************************************************************
        
        for key in self['pricing']:
            if self['pricing'][key][0] in DEVISEs:
                self['pricing'][key] = dict(
                    devise = self['pricing'][key][0],
                    value  = float(self['pricing'][key][1:]),
                )
        
        #*****************************************************************************************************************
        
        return self
    
    #######################################################################################################################
    
    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")
    
    #######################################################################################################################
    
    class Schema:
        collection = 'retail_article'
        unique_key = [
            ('site', pymongo.ASCENDING),
            ('uid',  pymongo.ASCENDING),
        ]
        mapping    = {
            'image_urls': None,
        }

