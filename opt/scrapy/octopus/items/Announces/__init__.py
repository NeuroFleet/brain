# -*- coding: utf-8 -*-

from octopus.utils import *

#****************************************************************************************

class JobOffer(NativeItem):
    source       = scrapy.Field()
    narrow       = scrapy.Field()
    
    title        = scrapy.Field()
    summary      = scrapy.Field()
    when         = scrapy.Field()
    
    company      = scrapy.Field(default={})
    details      = scrapy.Field(default={})
    
    class MongoDB:
        collection = 'job_offer'
        unique_key = [
            ('source', pymongo.ASCENDING),
            ('narrow', pymongo.ASCENDING),
        ]

