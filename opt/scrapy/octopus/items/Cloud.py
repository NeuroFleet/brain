# -*- coding: utf-8 -*-

from octopus.utils import *

#****************************************************************************************

class PhysicalServer(NativeItem):
    brand       = scrapy.Field()
    narrow      = scrapy.Field()
    
    pricing     = scrapy.Field(default={
        'ontime':  '0 MAD',
        'monthly': '',
    })
    
    specs_cpu   = scrapy.Field(default={})
    specs_ram   = scrapy.Field(default={})
    specs_hdd   = scrapy.Field(default={})
    specs_raid  = scrapy.Field(default={})
    specs_net   = scrapy.Field(default={})
    
    chassis     = scrapy.Field()
    extensions  = scrapy.Field()
    
    cover       = scrapy.Field()
    images      = scrapy.Field()
    
    def cleanup(self):
        if 'raw' in self['specs_cpu']:
            if ' vCores' in srv['specs_cpu']['raw']:
                srv['specs_cpu']['unit'], srv['specs_cpu']['count'] = 'vCore', int(srv['specs_cpu']['raw'].lower().replace('vcore','').replace(' s','').strip())
        
        for key in ('cores','threads'):
            if key in self['specs_cpu']:
                self['specs_cpu'][key] = int(self['specs_cpu'][key])
        
        for key in ('ram','hdd'):
            if (type(self['specs_'+key]) is dict) and ('raw' in self['specs_'+key]):
                lst = [
                    y
                    for sep in ('Go', 'To')
                    if sep in self['specs_'+key]['raw']
                    for y in [sep]+[
                        x.strip()
                        for x in self['specs_'+key]['raw'].split(sep)
                    ]
                ]
                
                if len(lst)==3:
                    self['specs_'+key]['unit'], self['specs_'+key]['size'], self['specs_'+key]['raw'] = lst
                
                if 'x' in self['specs_'+key]['size']:
                    self['specs_'+key]['count'], self['specs_'+key]['size'] = self['specs_'+key]['size'].strip().split('x')
                
                for field in ('size','count'):
                    self['specs_'+key][field] = int(self['specs_'+key].get(field, '') or '1')
                
                if 'raw' in self['specs_'+key]:
                    del self['specs_'+key]['raw']
                
                if 'count' in self['specs_'+key]:
                    if self['specs_'+key]['count']==1:
                        del self['specs_'+key]['count']
        
        #*****************************************************************************************************************
        
        for key in self['pricing']:
            if (type(self['pricing'][key]) in (str, unicode)) and len(self['pricing'][key]):
                if self['pricing'][key][0] in DEVISEs:
                    self['pricing'][key] = dict(
                        devise = self['pricing'][key][0],
                        value  = float(self['pricing'][key][1:]),
                    )
        
        #*****************************************************************************************************************
        
        return self
    
    #######################################################################################################################
    
    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")
    
    #######################################################################################################################
    
    class Schema:
        collection = 'cloud_server_phy'
        unique_key = [
            ('brand', pymongo.ASCENDING),
            ('narrow', pymongo.ASCENDING),
        ]

class VirtualServer(NativeItem):
    brand       = scrapy.Field()
    narrow      = scrapy.Field()
    
    pricing     = scrapy.Field(default={
        'ontime':  '0 MAD',
        'monthly': '',
    })
    
    specs_cpu   = scrapy.Field(default={})
    specs_ram   = scrapy.Field(default={})
    specs_hdd   = scrapy.Field(default={})
    specs_net   = scrapy.Field(default={})
    
    def cleanup(self):
        
        #*****************************************************************************************************************
        
        for key in self['pricing']:
            if self['pricing'][key][0] in DEVISEs:
                self['pricing'][key] = dict(
                    devise = self['pricing'][key][0],
                    value  = float(self['pricing'][key][1:]),
                )
        
        #*****************************************************************************************************************
        
        return self
    
    #######################################################################################################################
    
    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")
    
    #######################################################################################################################
    
    class Schema:
        collection = 'cloud_server_virt'
        unique_key = [
            ('brand', pymongo.ASCENDING),
            ('narrow', pymongo.ASCENDING),
        ]

class Domain(NativeItem):
    brand       = scrapy.Field()
    tld         = scrapy.Field()
    
    pricing     = scrapy.Field(default={
        'ontime':  '0 MAD',
        'monthly': '',
    })
    
    hosting_plans = scrapy.Field(default=[])
    
    #######################################################################################################################
    
    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")
    
    #######################################################################################################################
    
    class Schema:
        collection = 'cloud_domain_tld'
        unique_key = [
            ('brand', pymongo.ASCENDING),
            ('narrow', pymongo.ASCENDING),
        ]

class HostingPlan(NativeItem):
    brand         = scrapy.Field()
    narrow        = scrapy.Field()
    
    pricing     = scrapy.Field(default={
        'ontime':  '0 MAD',
        'monthly': '',
    })
    
    specs_disk    = scrapy.Field(default='1 Go')
    specs_traffic = scrapy.Field(default='1 To')
    specs_domains = scrapy.Field(default=0)
    specs_coloc   = scrapy.Field(default=1)
    specs_mail    = scrapy.Field(default=None)
    
    #######################################################################################################################
    
    class Validator:
        def check(self):
            pass # raise DropItem("Article is not validated !")
    
    #######################################################################################################################
    
    class Schema:
        collection = 'cloud_hosting_plan'
        unique_key = [
            ('brand', pymongo.ASCENDING),
            ('narrow', pymongo.ASCENDING),
        ]

