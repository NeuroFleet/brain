# -*- coding: utf-8 -*-

import re, scrapy

from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

from urlparse import urlparse, urljoin

#****************************************************************************************

from octopus.utils                  import *

from octopus.items.Cloud            import *
from octopus.items.Shopping.Magento import *
from octopus.items.Shopping.Retail  import *

#****************************************************************************************

def cleanup(selector, sep=None, trim=False, inline=False):
    resp = selector.extract()
    
    if sep is str:
        resp = [
            y
            for x in resp
            for y in unicode(x).split()
        ]
        
        sep = ''
    
    if sep is None:
        if len(resp):
            resp = resp[0]
        else:
            resp = ""
    else:
        resp = sep.join(resp)
    
    if inline:
        for spc in ['\n', '\t']:
            while spc in resp:
                resp = resp.replace(spc, ' ')
    
    if trim:
        if hasattr(resp, 'strip'):
            resp = resp.strip()
        else:
            resp = [x.strip() for x in resp]
    
    return unicode(resp).encode('utf-8')

