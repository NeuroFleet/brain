# -*- coding: utf-8 -*-

from octopus.shortcuts import *

from urlparse import urlparse, parse_qs as urlparse_qs

class JobsaumarocSpider(CrawlSpider):
    name = "jobsaumaroc"
    allowed_domains = ["jobsaumaroc.com", "www.jobsaumaroc.com"]
    
    start_urls = (
        'http://www.jobsaumaroc.com/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('jobs/(.*)', )), callback='parse_categ', follow=True),
        Rule(LinkExtractor(allow=('job/([\d]+)', )), callback='parse_offer', follow=True),
        Rule(LinkExtractor(allow=('job/([\d]+)/(.+)/', )), callback='parse_offer', follow=True),
    )
    
    def parse_categ(self, response):
        pass
    
    def parse_offer(self, response):
        entry = response
        
        offer = JobOffer()
        
        uri = urlparse(response.url)
        
        offer['link']          = response.url
        offer['source']        = self.name
        offer['narrow']        = response.url
        
        for key in ['http://','/job/'] + sorted(self.allowed_domains, reverse=True):
            while key in offer['narrow']:
                offer['narrow'] = offer['narrow'].replace(key, '')
        
        offer['narrow'] = offer['narrow'].split('/')[0]
        
        if offer['narrow']:
            offer['title']         = cleanup(entry.xpath('.//div[@id="job-details"]/h2/text()'), sep=None, trim=True)
            offer['summary']       = cleanup(entry.xpath('.//div[@id="job-description"]'), sep=None, trim=True)
            #offer['when']          = cleanup(entry.xpath('.//div[@id="view-right"]/span/text()'), sep=None, trim=True)
            
            offer['details'] = {}
            
            offer['company'] = {
                'name': cleanup(entry.xpath('.//div[@id="job-details"]/p[1]/strong[1]/text()'), sep=None, trim=True),
                'location': cleanup(entry.xpath('.//div[@id="job-details"]/p[1]/strong[2]/text()'), sep=None, trim=True),
            }
            
            if len(offer['company']['location'])==0 and len(offer['company']['name'])!=0:
                offer['company']['location'] = offer['company']['name']
                
                del offer['company']['name']
            
            if 'PROFIL RECHERCHE' in offer['summary']:
                offer['summary'], offer['details']['profile'] = offer['summary'].split('PROFIL RECHERCHE', 1)
            
            if 'RESPONSABILITIES' in offer['summary']:
                offer['summary'], offer['details']['responsabilities'] = offer['summary'].split('RESPONSABILITIES', 1)
            
            yield offer

