# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class SolostocksSpider(CrawlSpider):
    name = "solostocks"
    allowed_domains = ["solostocks.ma", "www.solostocks.ma"]
    
    start_urls = (
        'http://www.solostocks.ma/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('([\-\_\.\/\w\d]*)-([\d]+)', )), callback='parse_produit'),
    )
    
    def parse_produit(self, response):
        entry = MagentoArticle()
        
        entry['link']       = response.url
        entry['uid']        = cleanup(response.xpath('//*[@class="no-display"]/input[@name="product"]/@value'), sep=None, trim=True)
        
        entry['title']      = cleanup(response.xpath('//*[@class="product-name"]/h1/text()'), sep=None, trim=True)
        entry['excerpt']    = cleanup(response.xpath('//*[@class="short-description"]/text()'), sep='\n', trim=True)
        entry['summary']    = cleanup(response.xpath('//*[@class="product-collateral"]//*[@class="std"]/*'), sep='\n', trim=True)
        
        entry['price_ht']   = cleanup(response.xpath('//span[@class="price-excluding-tax"]/span[@class="price"]/text()'), sep=None, trim=True)
        entry['price_ttc']  = cleanup(response.xpath('//span[@class="price-including-tax"]/span[@class="price"]/text()'), sep=None, trim=True)
        
        entry['image_urls'] = response.xpath('//*[@class="product-view"]//img/@src').extract()
        
        return entry

