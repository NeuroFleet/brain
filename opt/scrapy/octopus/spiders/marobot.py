# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class MarobotSpider(CrawlSpider):
    name = "marobot"
    allowed_domains = ["store.marobot.com"]
    
    start_urls = (
        'http://store.marobot.com/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('index\.php\/(.+)\.html', )), callback='parse_article', follow=True),
    )
    
    def parse_article(self, response):
        nomenclature = [
            x
            for x in ' '.join(response.xpath('/'.join([
                '/', 'div[@class="col-main"]', 'div[starts-with(@class,"page-title")]', '@class'
            ])).extract()).replace('-title','').split(' ')
            if x not in ('page',)
        ]
        
        if 'category' in nomenclature:
            categ = RetailCategory()
            
            categ['site']       = self.name
            categ['link']       = response.url
            categ['uid']        = cleanup(response.xpath('//div[@class="col-main"]/div[starts-with(@class,"page-title")]/h1/text()'), sep=None, trim=True)
            categ['title']      = cleanup(response.xpath('//div[@class="col-main"]/div[starts-with(@class,"page-title")]/h1/text()'), sep=None, trim=True)
            
            #*****************************************************************************************************************
            
            categ['products']   = [
                {
                    'uid':   cleanup(product.xpath('.//h1/a/@href'), sep=None, trim=True, inline=True),
                    'title': cleanup(product.xpath('.//h1/a/text()'), sep=None, trim=True, inline=True),
                    'url':   cleanup(product.xpath('.//h1/a/@href'), sep=None, trim=True, inline=True),
                    'thumb': cleanup(product.xpath('.//img/@src'), sep=None, trim=True, inline=True),
                }
                for product in response.xpath('//*[@class="product-view"]')
            ]
            
            #*****************************************************************************************************************
            
            #categ['products']   = response.xpath('').extract()
            
            yield categ
        else:
            entry = RetailArticle()
            
            entry['site']            = self.name
            entry['link']            = response.url
            entry['uid']             = cleanup(response.xpath('//*[@class="no-display"]/input[@name="product"]/@value'), sep=None, trim=True)
            
            entry['title']           = cleanup(response.xpath('//*[@class="product-name"]/h1/text()'), sep=None, trim=True)
            entry['summary']         = cleanup(response.xpath('//*[@class="short-description"]/text()'), sep='\n', trim=True)
            entry['description']     = cleanup(response.xpath('//*[@class="product-collateral"]//*[@class="std"]/*'), sep='\n', trim=True)
            
            entry['pricing']         = dict(
                per_unit=cleanup(response.xpath('//span[@class="price"]/text()'), sep=None, trim=True),
            )
            
            entry['media'] = {
                'links': response.xpath('//*[@class="product-view"]//img/@src').extract(),
            }
            
            yield entry

