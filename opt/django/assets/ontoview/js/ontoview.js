// there is a conflict between tooltips and tree!!!



$(document).ready(function() {



	// http://stackoverflow.com/questions/3550133/how-to-get-the-jstree-work


	 $("#content").jstree({ 
	 		// the core plugin - not many options here
	 		"core" : { 
	 			 "animation" : 200,
	 			 "html_titles" : true,
	 		},
	 	
	  "themes": {
	         "theme": "ontoview",
	         "dots": true,
	         "icons": true,
	         // "url": "themes/default/style.css" 
	       },
	  "plugins" : [ "html_data", "themes" ], 
	 
	 });
	



    // http://flowplayer.org/tools/demos/tooltip/index.html

	$("[title]").tooltip({ 
		delay: 100, 
		predelay: 500,
		effect: 'fade', 
		// position: "bottom right", 
		position: "center right", 
		offset: [0, 15],
		opacity: 1
	});


	// make the tree collapsable / expandable
	
	var myfun = function() { $(".classblock > #classBody").slideUp(); };
	setTimeout(myfun, 1000);
	
	
	$(".resourceOpener").toggle(
		  function () {
			// $(this).next().slideUp("slow");
			$(this).parent().parent().parent().parent().parent().next().slideUp("slow");
		  }, 
		  function () {
			$(this).parent().parent().parent().parent().parent().next().slideDown("slow");
		  }
		);
	
    

});




function make_nextelement_collapsable(element) {
	"wrapper for the convenient toggle function"
	
	$(element).toggle(
		  function () {
			$(this).next().slideUp("slow");
		  }, 
		  function () {
			$(this).next().slideDown("slow");
		  }
		);
	
}

