jQuery(function () {
  jQuery([
    { source: 'docker',           target: 'cubes' },
    { source: 'launchpad',        target: 'connectdevelop' },
    { source: 'pocket',           target: 'get-pocket' },

    { source: 'bitbucket-oauth2', target: 'bitbucket' },
    { source: 'dropbox-oauth2',   target: 'dropbox' },
  ]).each(function (index, item) {
    jQuery('.fa-'+item.source).addClass('fa-'+item.target);
  });

  jQuery('a.social-dissociate').click(function (ev) {
    var entry = ev.currentTarget;

    jQuery(entry).parent().submit();
  });
});
