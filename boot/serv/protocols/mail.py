import os,sys

from datetime import datetime

from inbox import Inbox

inbox = Inbox()

@inbox.collate
def handle(to, sender, subject, body):
    print "[%s] (%s) -> (%s) : %s (%d o)" % (datetime.now(), sender, to, subject, len(body))

if True: # __name__=='__main__':
    port = 25 # int(os.environ.get('PORT', 25))

    print "[%s] Starting the server at port %d :" % (datetime.now(), port)

    inbox.serve(address='0.0.0.0', port=port)
