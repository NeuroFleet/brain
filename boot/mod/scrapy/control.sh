#!/bin/bash

if [[ "x$GHOST_PERSON" == "x" ]] ; then echo "Ghost person not defined. Try : export GHOST_PERSON=proto" ; return ; fi

export GHOST_TARGET=$SCRAPY_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9300 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            ;;
        release)
            ;;
        ########################################################################
        shell)
            ipython
            ;;
        invite)
            cd $SCRAPY_HOME

            runner scrapy shell -i bpython
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

