#!/bin/bash

export GHOST_TARGET=$CROSSBAR_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9100 ; fi

################################################################################

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            ;;
        release)
            ;;
        ########################################################################
        shell)
            bash

            runner ptpython
            ;;
        invite)
            ssh -p 6022 oberstet@127.0.0.1
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

