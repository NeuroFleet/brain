#!/bin/bash

for key in hubot external vtr ; do
    symlink $GHOST_SRV/$key-scripts.json $HUBOT_HOME/$key-scripts.json
done

ln -sf $GHOST_LIB/python/$GHOST_REACTOR/converse/*.coffee    $HUBOT_HOME/scripts/

ln -sf $GHOST_LIB/python/$GHOST_NARROW/*/converse/*.coffee $HUBOT_HOME/scripts/

