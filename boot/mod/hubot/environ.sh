export HUBOT_HOME=$GHOST_OPT/hubot

export HUBOT_AUTH_ADMIN=shivhack

export HUBOT_PARTYLINE_PORT=8879

#*******************************************************************************

export REDISTOGO_URL=$REDIS_URL
export MONGODB_URI=$MONGODB_URL

################################################################################

export GOOGLE_API_PRIVATE_KEY=771fc22fec0d5404139d6002f0a17db0c2aeaeb5
export GOOGLE_API_CLIENT_EMAIL=hubot-81@uchikoma-proto.iam.gserviceaccount.com

#*******************************************************************************

export HUBOT_TWITTER_CONSUMER_KEY=$API_TWITTER_KEY
export HUBOT_TWITTER_CONSUMER_SECRET=$API_TWITTER_PKI
export HUBOT_TWITTER_ACCESS_TOKEN=$API_TWITTER_KEY
export HUBOT_TWITTER_ACCESS_TOKEN_SECRET=$API_TWITTER_PKI

#*******************************************************************************

export HUBOT_INSTAGRAM_CLIENT_KEY=$API_INSTAGRAM_KEY
export HUBOT_INSTAGRAM_ACCESS_KEY=$API_INSTAGRAM_PKI

#*******************************************************************************

export HUBOT_STACK_OVERFLOW_API_KEY=$API_STACKOVERFLOW_KEY

################################################################################

export HUBOT_BITBUCKET_PUSH_URL="/bitbucket/push"
export HUBOT_BITBUCKET_PUSH_EVENT=bitbucketPushReceived

export HUBOT_REDMINE_BASE_URL="http://redmine.your-server.com"
export HUBOT_REDMINE_TOKEN="your api token here"

#*******************************************************************************

export HUBOT_AIRTIME_URL=http://airtime.myhost.com

#*******************************************************************************

export HUBOT_WORDPRESS_URL="http://yourwordpressblog.com"
export HUBOT_WORDPRESS_QUERY_URL="http://yourwordpressblog.com/?json=get_search_results&s="
export HUBOT_WORDPRESS_QUERY_MIN_CHARS=3
export HUBOT_WORDPRESS_QUERY_MAX_RESULTS=5
export HUBOT_WORDPRESS_RESPOND_TRIGGER=wp
export HUBOT_WORDPRESS_HEAR_TRIGGERS="where is;where are"
export HUBOT_WORDPRESS_SITE_NAME="knowledge base"

export HUBOT_TRELLO_KEY="Trello application key"
export HUBOT_TRELLO_TOKEN="Trello API token"
export HUBOT_TRELLO_BOARD="The ID of the Trello board you will be working with"

export ASANA_WORKSPACE_ID="Your workspace's ID."
export ASANA_TEAM_ID="Your team's ID."
export ASANA_DEFAULT_PROJECT_ID="Your default project's ID."
export ASANA_DEFAULT_PROJECT_NAME="Your default project's name."

#   HUBOT_WUNDERLIST_SMTP_HOST - your smtp host e.g. smtp.gmail.com
#   HUBOT_WUNDERLIST_SMTP_PORT - the port to connect to
#   HUBOT_WUNDERLIST_SMTP_USESSL - whether you want to connect via SSL
#   HUBOT_WUNDERLIST_SMTP_SENDDOMAIN - the domain from which to send
#   HUBOT_WUNDERLIST_SMTP_USEAUTH - BOOL: authentication required
#   HUBOT_WUNDERLIST_SMTP_AUTH_NAME - username for authentication
#   HUBOT_WUNDERLIST_SMTP_AUTH_PASSWORD  - password for authentication

################################################################################

export HUBOT_DARK_SKY_API_KEY=
export HUBOT_DARK_SKY_DEFAULT_LOCATION=
export HUBOT_DARK_SKY_SEPARATOR=

