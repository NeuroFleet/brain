#!/bin/bash

export GHOST_TARGET=$SOLID_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9700 ; fi

################################################################################

export SSL_PREFIX=$GHOST_ETC/ssl/$GHOST_NARROW"-"$GHOST_PERSON

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            solid init
            ;;
        release)
            for app in signup profile explorer browser editor ; do
                if [[ ! -d $SOLID_BDIR/$app ]] ; then
                    git clone https://github.com/Solid-Warrior/$app.git $SOLID_BDIR/$app
                fi
            done
            #*******************************************************************************
            for app in contacts inbox notes tasks ; do
                if [[ ! -d $SOLID_BDIR/$app ]] ; then
                    git clone https://github.com/Solid-Warrior/app-$app.git $SOLID_BDIR/$app
                fi
            done
            #*******************************************************************************
            #*******************************************************************************
            for app in blog publish meetup timeline people ; do
                if [[ ! -d $SOLID_HOME/apps/$app ]] ; then
                    git clone https://github.com/Solid-Warrior/app-$app.git $SOLID_HOME/apps/$app
                fi
            done
            #*******************************************************************************
            #*******************************************************************************
            for app in clip video music ; do
                if [[ ! -d $SOLID_HOME/player/$app ]] ; then
                    git clone https://github.com/Solid-Warrior/play-$app.git $SOLID_HOME/player/$app
                fi
            done
            #*******************************************************************************
            for app in chess 2048 ; do
                if [[ ! -d $SOLID_HOME/games/$app ]] ; then
                    git clone https://github.com/Solid-Warrior/game-$app.git $SOLID_HOME/games/$app
                fi
            done
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

