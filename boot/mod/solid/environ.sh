export SOLID_HOME=$GHOST_OPT/solid
export SOLID_ROOT=$GHOST_SRV/$GHOST_NARROW/identity

export SOLID_FQDN=idp.$GHOST_DOMAIN
export SOLID_BDIR=$SOLID_ROOT/$SOLID_FQDN

export SSL_VAULT=$GHOST_ETC/ssl/$SOLID_FQDN

################################################################################

if [[ ! -f $SSL_VAULT.key  ]] ; then
    openssl genrsa 2048 > $SSL_VAULT.key
fi

#*******************************************************************************

if [[ ! -f $SSL_VAULT.cert ]] ; then
    openssl req -new -x509 -nodes -sha256 -days 3650 -key $SSL_VAULT.key -subj '/CN=*.vault.'$GHOSTY_DOMAIN > $SSL_VAULT.cert
fi

