#!/bin/bash

if [[ "x$GHOST_PERSON" == "x" ]] ; then echo "Ghost person not defined. Try : export GHOST_PERSON=proto" ; return ; fi

export GHOST_TARGET=$SOLID_HOME

if [[ "x"$PORT == "x" ]] ; then export PORT=9800 ; fi

################################################################################

export SSL_PREFIX=$GHOST_ETC/ssl/$GHOST_NARROW"-"$GHOST_PERSON

sysV_ctrl () {
    case $1 in
        clean)
            ;;
        build)
            solid init
            ;;
        release)
            ;;
        ########################################################################
        *)
            echo "Not implemented !"
            ;;
    esac
}

