#!/bin/bash

symlink $GHOST_ETC/airflow.cfg $AIRFLOW_HOME/airflow.cfg

symlink $GHOST_LIB/python/$GHOST_REACTOR/flow/login $AIRFLOW_HOME/airflow_login

ln -sf $GHOST_LIB/python/$GHOST_REACTOR/flow/dags/*.py $AIRFLOW_HOME/dags/
ln -sf $GHOST_LIB/python/$GHOST_NARROW/*/dags/*.py   $AIRFLOW_HOME/dags/
ln -sf $GHOST_SPECS/airflow/pipelines/*.py      $AIRFLOW_HOME/dags/

ln -sf $GHOST_LIB/python/$GHOST_REACTOR/flow/plugins/*.py $AIRFLOW_HOME/plugins/
ln -sf $GHOST_SPECS/airflow/extensions/*.py        $AIRFLOW_HOME/plugins/

