export LUIGI_HOME=$GHOST_OPT/luigi

export LUIGI_LOGS=$GHOST_VAR/logs/luigi
export LUIGI_TEMP=$GHOST_VAR/run/luigi

export PYTHONPATH=$LUIGI_HOME":"$PYTHONPATH

