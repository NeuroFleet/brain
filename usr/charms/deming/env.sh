#!/bin/sh

case "$RONIN_OS" in
  ubuntu|debian|raspbian)
    export APT_PKGs="$APT_PKGs httrack git"
    ;;
esac

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(atom web-search)
  ZSH_PLUGINs+=(gitfast git-extras git-flow git-hubflow)
fi

#*******************************************************************************

# Git-focused shortcuts

alias gs="git status"
alias gC="ga . && gc -m "
alias gd="git difftool "
alias gl="git log --graph --abbrev-commit --decorate --date=relative --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all | tac"

#*******************************************************************************

# Developer-oriented aliases

alias aspire='httrack -w -c8 -Rck -R5 -n -F "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:27.0) Gecko/20100101 Firefox/27.0" -%v2 -v "-*p3" -N0 -s15 -n'
alias openyourmind='git submodule foreach --recursive git pull -u origin master'

#*******************************************************************************

# Deming's Shell Cheatsheet

alias settle='deming settle'
alias commit='deming commit'
alias ship='deming ship'
alias devenv='deming env'
alias giting='deming sync'
alias deploy='deming deploy'

#*******************************************************************************

export STRIDER_PATH=$RONIN_CHARMS_DIR"/deming/src/Strider"

alias strider=$STRIDER_PATH"/bin/strider"
