Shell environment :
===================

Environment variables :
-----------------------

* RONIN_ROOT : The Shelter's root path.
* RONIN_PATH : The Shelter's system path for executables.

* RONIN_OS : Tells on which operating system the Shelter is running.

* RONIN_OS : Tells on which operating system the Shelter is running.

export NOH_ROOT="/ron/usr"
export RONIN_ROOT="/ron"

export RONIN_PATH=$RONIN_ROOT'/sbin'

export RONIN_OS="debian"

export RONIN_HOME_ROOT=/root
export RONIN_HOME_DIR=/home

export RONIN_CHARMS_DIR=$RONIN_ROOT/usr/charms

Shell aliases :
---------------

```sh
alias cloud9=$CLOUD9_PATH"/bin/cloud9.sh"

cloud9_workspace () {
    cloud9-cli -w $CLOUD9_WORKSPACEs/$1 -l 0.0.0.0 -p $2 # --username $3 --password $4
}
```

Debian packages :
-----------------

* ant
