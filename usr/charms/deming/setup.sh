#!/bin/sh

if [[ -d "$STRIDER_PATH" ]] ; then
    cd $STRIDER_PATH

    npm install
fi
