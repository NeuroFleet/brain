#!/bin/sh

case "$RONIN_OS" in
  ubuntu|debian|raspbian)
    export APT_PKGs="$APT_PKGs "
    ;;
esac

#*******************************************************************************

if [[ $ZSH != '' ]] ; then
  ZSH_PLUGINs+=(sprunge)
  ZSH_PLUGINs+=(aws github)
  ZSH_PLUGINs+=(heroku cloudapp)
fi

#*******************************************************************************
