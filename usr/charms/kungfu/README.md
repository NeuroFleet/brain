# Shell commands #

* voodoo :
* zombie :

# Features #

* Flavor detection on repos dynamically under specific path naming convention.
* C.I & DevOps oriented annotation of vhosts :
  - CDN
  - API
  - Mail
  - CRON
  - Instances
  - Bots
  - Backends
  - Shipping
  - ACL

# Local hosting #

The local hosting feature is provided using the Heroku Cedar container. You can download it separatley using :

```sh
docker pull heroku/cedar
```

### List of packages ###

* apache2
* tomcat7
* jboss-as4
* grails
* rails
* gunicorn
* JAX-RS
