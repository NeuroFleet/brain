#!/bin/bash

cd /app

if [[ -e Procfile ]] ; then
  foreman start
else
  if [[ -e requirements.txt ]] ; then
    echo Not a git repository. Existing ...
    exit 1
  else
    if [[ -e pom.xml ]] ; then
      echo Not a git repository. Existing ...
      exit 1
    else
      if [[ -e composer.json ]] ; then
        service apache2 start

        tail -f /var/log/apache2/access.log /var/log/apache2/error.log
      fi
    fi
  fi
fi
