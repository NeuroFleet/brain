#!/bin/bash

if [[ ! -d /app ]] ; then
  git clone https://github.com/heroku/python-sample.git /app
fi

if [[ ! -d /app/.git ]] ; then
  echo Not a git repository. Existing ...
  exit 1
fi

cd /app

if [[ -e requirements.txt ]] ; then
  if [[ ! -d venv ]] ; then
    virtualenv --distribute venv/

    venv/bin/pip install -U pip distribute
  fi

  venv/bin/pip install -r requirements.txt

  if [[ ! -e Procfile ]] ; then
    if [[ -e manage.py ]] ; then
      echo "web: python manage.py runserver 0.0.0.0:80"
    else
      echo Not a git repository. Existing ...
      exit 1
    fi
  fi
else
  if [[ -e pom.xml ]] ; then
    mvn clean

    mvn build

    if [[ ! -e Procfile ]] ; then
      echo Not a git repository. Existing ...
      exit 1
    fi
  else
    if [[ -e composer.json ]] ; then
      composer install
    fi
  fi
fi
