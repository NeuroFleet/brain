#!/bin/bash

rm -fR venv vendor

rm -fR /etc/apache2

cp -afR /kata/config/apache2 /etc/apache2
