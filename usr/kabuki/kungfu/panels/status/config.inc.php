<?php

defined('IN_INFO') or exit;

// If you experience timezone errors, uncomment (remove //) the following line and change the timezone to your liking
// date_default_timezone_set('America/New_York');

$settings['byte_notation'] = 1024; // Either 1024 or 1000; defaults to 1024
$settings['dates'] = 'm/d/y h:i A (T)'; // Format for dates shown. See php.net/date for syntax
$settings['language'] = 'en'; // Refer to the lang/ folder for supported lanugages
$settings['icons'] = true; // simple icons 

$settings['show']['kernel'] = true;
$settings['show']['os'] = true;
$settings['show']['load'] = true;
$settings['show']['ram'] = true;
$settings['show']['hd'] = true;
$settings['show']['mounts'] = true;
$settings['show']['mounts_options'] = false; // Might be useless/confidential information; disabled by default.
$settings['show']['network'] = true;
$settings['show']['uptime'] = true;
$settings['show']['cpu'] = true;
$settings['show']['process_stats'] = true; 
$settings['show']['hostname'] = true;
$settings['show']['distro'] = true; # Attempt finding name and version of distribution on Linux systems
$settings['show']['devices'] = true; # Slow on old systems
$settings['show']['model'] = true; # Model of system. Supported on certain OS's. ex: Macbook Pro
$settings['show']['numLoggedIn'] = true; # Number of unqiue users with shells running (on Linux)
$settings['show']['duplicate_mounts'] = true;
$settings['show']['temps'] = false;
$settings['show']['raid'] = false; 
$settings['show']['battery'] = false;
$settings['show']['sound'] = false;
$settings['show']['wifi'] = false; # Not finished
$settings['show']['services'] = false;

$settings['hide']['filesystems'] = array(
	'tmpfs', 'ecryptfs', 'nfsd', 'rpc_pipefs',
	'usbfs', 'devpts', 'fusectl', 'securityfs', 'fuse.truecrypt');
$settings['hide']['storage_devices'] = array('gvfs-fuse-daemon', 'none');

$settings['hide']['fs_mount_options'] = array('ecryptfs');
$settings['hide']['sg'] = true; # Linux only

$settings['raid']['gmirror'] = false;  # For FreeBSD
$settings['raid']['mdadm'] = false;  # For Linux; known to support RAID 1, 5, and 6

$settings['temps']['hwmon'] = true;
$settings['temps']['hddtemp'] = true;
$settings['temps']['mbmon'] = false;
$settings['temps']['sensord'] = false;

$settings['hddtemp']['mode'] = 'daemon'; // Either daemon or syslog
$settings['hddtemp']['address'] = array( // Address/Port of hddtemp daemon to connect to
	'host' => 'localhost',
	'port' => 7634
);
// Configuration for getting temps with mbmon
$settings['mbmon']['address'] = array( // Address/Port of mbmon daemon to connect to
	'host' => 'localhost',
	'port' => 411
);

/*
 * For the things that require executing external programs, such as non-linux OS's
 * and the extensions, you may specify other paths to search for them here:
 */
$settings['additional_paths'] = array(
	 //'/opt/bin' # for example
);

$settings['services']['pidFiles'] = array(
	'Apache2' => '/var/run/apache2.pid', // uncomment to enable
	'SSHd' => '/var/run/sshd.pid'
);

$settings['services']['executables'] = array(
	'MySQLd' => '/usr/sbin/mysqld' // uncomment to enable
	// 'BuildSlave' => array('/usr/bin/python', // executable
	//						1 => '/usr/local/bin/buildslave') // argv[1]
);

$settings['show_errors'] = false;
$settings['timer'] = true;
$settings['compress_content'] = true;

$settings['sudo_apps'] = array(
	//'ps' // For example
);

