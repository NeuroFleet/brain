#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",mysql"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
#fi

if [ $SHL_OS=="ubuntu" ] ; then
    export APT_PKGs="$APT_PKGs nginx-full apache2-mpm-prefork `echo libapache2-mod-{auth-pam,jk,mono,php5,python,wsgi,bw,geoip,rpaf}`"
    export APT_PKGs=$APT_PKGs" memcached rabbitmq-server mysql-server"
fi

alias zombie-clone='voodoo clone'
alias zombie-list='voodoo list'
alias zombie-info='voodoo -v list'

alias zombie-enable='voodoo enable'
alias zombie-disable='voodoo disable'
alias zombie-status='voodoo status'

alias zombie-reload='voodoo reload'
alias zombie-start='voodoo start'
alias zombie-restart='voodoo restart'
alias zombie-stop='voodoo stop'
