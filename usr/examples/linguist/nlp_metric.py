>>> from pattern.metrics import similarity, levenshtein
>>> 
>>> print similarity('cat', 'what')
>>> print levenshtein('cat', 'what')
 
0.5
2

>>> from pattern.metrics import readability
>>> 
>>> dr_seuss = "\n".join((
>>>     "'I know some good games we could play,' said the cat.",
>>>     "'I know some new tricks,' said the cat in the hat.",
>>>     "'A lot of good tricks. I will show them to you.'",
>>>     "'Your mother will not mind at all if I do.'"
>>> ))
>>> print readability(dr_seuss)
 
0.908

>>> from pattern.metrics import cooccurrence
>>>
>>> f = open('pattern/test/corpora/tagged-en-oanc.txt')
>>> m = cooccurrence(f, 
>>>        window = (-2, -1),
>>>         term1 = lambda w: w[1] == 'NN',
>>>         term2 = lambda w: w[1] == 'JJ',
>>>     normalize = lambda w: tuple(w.split('/')) # cat/NN => ('cat', 'NN') 
>>> )
>>> for noun in m:
>>>     for adjective, count in m[noun].items():
>>>         print adjective, noun, count
 
('last', 'JJ') ('year', 'NN') 31
('next', 'JJ') ('year', 'NN') 10
('past', 'JJ') ('year', 'NN') 7


