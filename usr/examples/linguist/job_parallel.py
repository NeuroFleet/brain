import tempfile
import shutil
import os
import numpy as np

from joblib import Parallel, delayed
from joblib import load, dump

class MyCustomBackend(ParallelBackendBase):

    def __init__(self, endpoint, api_key):
       self.endpoint = endpoint
       self.api_key = api_key

    ...
    # Do something with self.endpoint and self.api_key somewhere in
    # one of the method of the class

register_parallel_backend('custom', MyCustomBackend)

with parallel_backend('custom', endpoint='http://compute', api_key='42'):
    Parallel()(delayed(some_function)(i) for i in range(10))

    with Parallel(n_jobs=2) as parallel:
       accumulator = 0.
       n_iter = 0
       while accumulator < 1000:
           results = parallel(delayed(sqrt)(accumulator + i ** 2)
                              for i in range(5))
           accumulator += sum(results)  # synchronization barrier
           n_iter += 1

