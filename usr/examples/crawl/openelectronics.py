# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class OpenelectronicsSpider(CrawlSpider):
    name = "openelectronics"
    allowed_domains = ["store.open-electronics.org"]
    
    start_urls = (
        'https://store.open-electronics.org/',
    )
    
    rules = (
        Rule(LinkExtractor(allow=('(.*)', )),    callback='parse_page',   follow=True),
    )
    
    #######################################################################################################################
    
    def parse_page(self, response):
        if len(response.xpath('//div[@class="product-info"]')):
            entry = RetailArticle()
            
            entry['site']            = self.name
            entry['link']            = response.url
            entry['uid']             = cleanup(response.xpath('//input[@name="product_id"]/@value'), sep=None, trim=True)
            
            entry['specs']           = {
                'brand': {
                    'name': cleanup(response.xpath('//div[@class="product-info"]//div[@class="description"]/span[1]/following-sibling::a[1]/text()'), sep=None, trim=True),
                    'link': cleanup(response.xpath('//div[@class="product-info"]//div[@class="description"]/span[1]/following-sibling::a[1]/@href'), sep=None, trim=True),
                },
                'code':     cleanup(response.xpath('//div[@class="product-info"]//div[@class="description"]/span[2]/following-sibling::text()[1]'), sep=None, trim=True),
            }
            
            entry['title']           = cleanup(response.xpath('//div[@id="content"]/h1/text()'), sep=None, trim=True)
            entry['description']     = cleanup(response.xpath('//div[@id="tab-description"]//text()'), sep='\n', trim=True)
            
            entry['stock']         = dict(
                status=cleanup(response.xpath('//div[@class="product-info"]//div[@class="description"]/span[3]/following-sibling::text()[1]'), sep=None, trim=True),
            )
            
            entry['pricing']         = dict(
                per_unit=cleanup(response.xpath('//div[@class="price"]/text()'), sep=None, trim=True).replace('Price:','').replace('\xe2\x82\xac','').strip(),
            )
            
            for sym,crn in [
                ('\u20ac', 'EUR'),
            ]:
                for key in entry['pricing']:
                    if sym in entry['pricing'][key]:
                        entry['pricing'][key] = dict(
                            value = float(entry['pricing'][key].replace(sym,'').replac(',','.')),
                            devise = crn,
                        )
            
            entry['media'] = {
                'links': response.xpath('//a[@rel="fancybox"]//img/@src').extract(),
            }
            
            yield entry

