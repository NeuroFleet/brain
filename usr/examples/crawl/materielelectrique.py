# -*- coding: utf-8 -*-

from octopus.shortcuts import *

class MaterielelectriqueSpider(CrawlSpider):
    name = "materielelectrique"
    allowed_domains = ["www.materielelectrique.com"]

    start_urls = (
        'https://www.materielelectrique.com/',
    )

    rules = (
        Rule(LinkExtractor(allow=('(.*)-c-(.*)\.html', )),                callback='parse_categ',   follow=True),
        Rule(LinkExtractor(allow=('(.*)-p-(.*)\.html', )),                callback='parse_product', follow=True),
    )

    def parse_categ(self, response):
        categ = RetailCategory()

        categ['link'] = response.url
        categ['site'] = self.name
        categ['uid']  = urlparse(response.url).path.split('-')[-1].replace('.html','')

        categ['title']      = cleanup(response.xpath('//div[@id="bread_piece"]/a/text()'), sep=None, trim=True)

        categ['products']   = [
            {
                'title':  cleanup(product.xpath('.//a[@class="productsListing"]/text()'), sep=None, trim=True),
                'url':    cleanup(product.xpath('.//a[@class="productsListing"]/@href'), sep=None, trim=True),
            }
            for product in response.xpath('//div[@id="boxListing"]')
        ]

        yield categ

    def parse_product(self, response):
        entry = RetailArticle()

        entry['link'] = response.url
        entry['site'] = self.name
        entry['uid']  = urlparse(response.url).path.split('-')[-1].replace('.html','')

        entry['vendor'] = dict(
            name  = cleanup(response.xpath('//div[@id="product_infos"]/text()[2]'), sep=None, trim=True).replace(':','').strip(),
            ref   = cleanup(response.xpath('//div[@id="product_infos"]/text()[3]'), sep=None, trim=True).replace(':','').strip(),
            ean   = cleanup(response.xpath('//div[@id="product_infos"]/text()[4]'), sep=None, trim=True).replace(':','').strip(),
        )

        entry['title']       = cleanup(response.xpath('//div[@id="product"]//h1[1]/text()'), sep=None, trim=True)
        entry['description'] = cleanup(response.xpath('//div[@id="description"]//text()'), sep='\n', trim=True)

        entry['stock']       = {
            'state':         cleanup(response.xpath('//span[@class="stockBig"]/text()'), sep=None, trim=True).lower(),
            'minimal-order': cleanup(response.xpath('//input[@id="minorder"]/@value'), sep=None, trim=True).lower(),
        }

        #if 'en stock' in entry['stock']['state']:
        #    entry['stock']['count'] = entry['stock']['state'].replace('in','').replace('stock','').strip()
        #    entry['stock']['state'] = 'availible'

        entry['pricing']         = dict(
            per_unit={
                'devise': 'EUR',
                'value':  cleanup(response.xpath('//span[@class="price"]/text()'), sep=None, trim=True).replace('£',''),
            },
        )

        entry['media'] = {
            'cover': cleanup(response.xpath('//a[@class="lightbox"]/img/@src'), sep=None, trim=True),
        }

        if entry['uid']:
            yield entry

