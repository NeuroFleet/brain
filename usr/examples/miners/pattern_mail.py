>>> from pattern.web import Mail, GMAIL, SUBJECT
>>> 
>>> gmail = Mail(username='...', password='...', service=GMAIL)
>>> print gmail.folders.keys()
 
['drafts', 'spam', 'personal', 'work', 'inbox', 'mail', 'starred', 'trash']


>>> from pattern.web import Facebook, NEWS, COMMENTS, LIKES
>>> 
>>> fb = Facebook(license='your key')
>>> me = fb.profile(id=None) # user info dict
>>> 
>>> for post in fb.search(me['id'], type=NEWS, count=100):
>>>     print repr(post.id)
>>>     print repr(post.text)
>>>     print repr(post.url)
>>>     if post.comments > 0:
>>>         print '%i comments' % post.comments 
>>>         print [(r.text, r.author) for r in fb.search(post.id, type=COMMENTS)]
>>>     if post.likes > 0:
>>>         print '%i likes' % post.likes 
>>>         print [r.author for r in fb.search(post.id, type=LIKES)]
 
u'530415277_10151455896030278'
u'Tom De Smedt likes CLiPS Research Center'
u'http://www.facebook.com/CLiPS.UA'
1 likes 
[(u'485942414773810', u'CLiPS Research Center')]

