#export NODE_PATH=$GHOST_LIB"/nodejs/dist"

if [[ -d $GHOST_FOLDER/node_modules/.bin ]] ; then
    export PATH=$PATH:$GHOST_FOLDER/node_modules/.bin
fi

if [[ -d $GHOST_TARGET/node_modules/.bin ]] ; then
    export PATH=$PATH:$GHOST_FOLDER/node_modules/.bin
fi

