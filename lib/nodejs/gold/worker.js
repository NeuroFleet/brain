var Worker = require('coffeeq').Worker;
var Jobs = require('./jobs');

var worker = new Worker(Jobs.queue, Jobs.listing);

worker.on('message', function(worker, queue){
  console.log("message fired");
});
worker.on('job', function(worker, queue){
  console.log("job fired");
});
worker.on('error', function(worker, queue){
  console.log("error fired");
});
worker.on('success', function(worker, queue){
  console.log("success fired");
});

worker.start();
