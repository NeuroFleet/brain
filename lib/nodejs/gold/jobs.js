exports.queue = 'messaging';

exports.listing = {
  succeed: function(callback) {
    console.log("callback succeed");
    callback();
  },
  fail: function(callback) {
    console.log("callback fail");
    callback();
  },
  multiply: function(a,b,callback) {
    console.log('callback multiply' + a + ' * ' + 'b');
    callback(a*b);      
  }
}
