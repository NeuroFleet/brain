#!/bin/bash

if [ $RONIN_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",nano"
    export CYG_PKGs=$CYG_PKGs",bash,dash,fish,mksh,zsh"
    export CYG_PKGs=$CYG_PKGs",tree,htop,nmap"
fi

if [ $RONIN_OS=="macosx" ] ; then
    export BREW_PKGs=$BREW_PKGs" mksh"
fi

if [ $RONIN_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" mksh zsh zsh-lovers"
    export APT_PKGs=$APT_PKGs" tree htop fping nmap traceroute"
fi

export NPM_PKGs="$NPM_PKGs nsh"

