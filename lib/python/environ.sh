export GHOST_VENV=$GHOST_VAR/pyenv

export PATH=$PATH:$GHOST_VENV/bin

source $GHOST_VENV/bin/activate

#*******************************************************************************

export EVENTLET_HUB=epolls

#*******************************************************************************

alias python='/usr/bin/python2.7'

alias vpip=$GHOST_VENV'/bin/pip'
alias vpython=$GHOST_VENV'/bin/python'

################################################################################


if [[ "x$PYTHONPATH" == "x" ]] ; then
    if [[ -d $GHOST_VENV ]] ; then
        export PYTHONPATH="$GHOST_VENV/lib/python2.7/site-packages"
    else
        export PYTHONPATH="/usr/local/lib/python2.7/dist-packages"
    fi
fi

#*******************************************************************************

if [[ -d $GHOST_SPECS/codes ]] ; then
    export PYTHONPATH=$PYTHONPATH":"$GHOST_SPECS"/codes"
fi

for key in dist gold ; do
    if [[ -d $GHOST_LIB/python/$key ]] ; then
        export PYTHONPATH=$PYTHONPATH":"$GHOST_LIB"/python/"$key
    fi
done

