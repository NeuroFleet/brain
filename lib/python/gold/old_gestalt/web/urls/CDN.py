from django.conf.urls        import patterns, include, url

from django.conf.urls.static import static

from uchikoma                import settings
from uchikoma.landing.views  import Status

urlpatterns = [
    url(r'^status/cloud\.json$',    Status.clouds,                 name='cloud'),
    url(r'^status/backends\.json$', Status.backends,               name='backends'),
    url(r'^status/platform\.json$', Status.platform,               name='platforms'),

    url(r'^status/$',               Status.general,                name='status'),

    url(r'^$',                      Status.homepage,               name='homepage'),
] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
) + static(
    settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)
