from django.conf.urls import patterns, include, url

#*******************************************************************************

from django.contrib import admin
admin.autodiscover()

#from neo4django import admin as neo_admin
#neo_admin.autodiscover()

################################################################################

urlpatterns = [
    url(r'^',          include('admin_honeypot.urls', namespace='admin_honeypot')),
]

