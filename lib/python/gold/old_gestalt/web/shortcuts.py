from gestalt.web.utils import *

from gestalt.core.shortcuts import *

#*******************************************************************************

from gestalt.web.helpers import Reactor

from django.conf import settings

Reactor.setup(settings)

#*******************************************************************************

from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import redirect

from django.template import RequestContext
from django.shortcuts import render_to_response

from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout, login

from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.apps.django_app.utils import psa

#*******************************************************************************

def context(*origins, **payload):
    origins = [o for o in origins]+[payload]

    resp = {}

    for target in origins:
        if type(target) in [dict]:
            resp.update(target)

    return resp

#*******************************************************************************

def render_to(tpl):
    def decorator(func):
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            out = func(request, *args, **kwargs)
            if isinstance(out, dict):
                out = render_to_response(tpl, out, RequestContext(request))
            return out
        return wrapper
    return decorator

