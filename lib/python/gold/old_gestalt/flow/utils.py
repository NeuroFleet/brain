from apps.deps import *

################################################################################

class ORM(object):
    REGISTRY = [
        dict(key='mongo', env='MONGOLAB_URI',   handler=MongoManager),
        dict(key='graph', env='GRAPHENEDB_URL', handler=GraphManager),
        dict(key='rdf',   env='RDF_NAMESPACE',  handler=RdfManager),
    ]

    @classmethod
    def detect(cls, parent):
        resp = {}
        
        for cfg in ORM.REGISTRY:
            if cfg['env'] in os.environ:
                entry = cfg['handler'](parent, cfg['key'], os.environ[cfg['env']])

                self._orm[entry.alias] = entry

        return resp

    #***************************************************************************

    def trigger(self, method, *args, **kwargs):
        hnd = getattr(self, method , None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            return None

    #***************************************************************************

    def __init__(self, parent, key, link):
        self._prn = parent
        self._key = key
        self._lnk = urlparse(link)

        self._cnx = None

        self.trigger('prepare', self.link)

    reactor = property(lambda self: self._prn)
    alias   = property(lambda self: self._key)
    link    = property(lambda self: self._lnk)

    #***************************************************************************

    @property
    def conn(self):
        if self._cnx is None:
            self._cnx = self.trigger('connect')

        return self._cnx

################################################################################

class MongoManager(ORM):
    def prepare(self, link):
        pass

    #***************************************************************************

    def connect(self):
        return Mongo.connect('default', host=self.url)

    ############################################################################

    def register_meta(self, *args, **kwargs):
        def do_apply(handler, alias):
            if handler is not None:
                if not hasattr(handler, 'meta'):
                    setattr(handler, 'meta', {})

                if not handler.meta.get('allow_inheritance', False):
                    handler.meta['allow_inheritance'] = True

            return handler

        return lambda hnd: do_apply(*args, **kwargs)

    #***************************************************************************

    def register_embed(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(*args, **kwargs)

    #***************************************************************************

    def register_schema(self, *args, **kwargs):
        def do_apply(handler, alias):
            return handler

        return lambda hnd: do_apply(*args, **kwargs)

    ############################################################################

    STD_MAP = {
        'bool': 'boolean',
        'int':  'integer',
        'str':  'string',
    }

    STD_MAP = {
        'char': 'string',
    }

################################################################################

class GraphManager(ORM):
    def prepare(self, link):
        pass

    #***************************************************************************

    def connect(self):
        return bulbs.neo4jserver.Graph(bulbs.neo4jserver.Config(self.link, self.link.username, self.link.password))

    ############################################################################

    nodes  = property(lambda self: self.conn.nodes)
    egdes  = property(lambda self: self.conn.edges)

    labels = property(lambda self: self.conn.labels)

    #***************************************************************************

    def query(self):
        req = requests.POST()

        resp = req.json()

        return resp

    ############################################################################

    def register_node(self, *args, **kwargs):
        def do_apply(handler, alias):
            setattr(handler, 'element_type', alias)

            self.graph.add_proxy(alias, handler)

            return handler

        return lambda hnd: do_apply(*args, **kwargs)

    #***************************************************************************

    def register_edge(self, *args, **kwargs):
        def do_apply(handler, alias):
            setattr(handler, 'label', alias)

            self.graph.add_proxy(alias, handler)

            return handler

        return lambda hnd: do_apply(*args, **kwargs)

################################################################################

class LinkedManager(ORM):
    def prepare(self, link):
        pass

    #***************************************************************************

    def connect(self):
        return rdflib.

    ############################################################################

    namespace = property(lambda self: self.conn.nodes)
    egdes  = property(lambda self: self.conn.edges)

    labels = property(lambda self: self.conn.labels)

    #***************************************************************************

    def query(self):
        req = requests.POST()

        resp = req.json()

        return resp

    ############################################################################

    def register_onto(self, *args, **kwargs):
        def do_apply(handler, alias):
            #self.graph.add_proxy(alias, handler)

            return handler

        return lambda hnd: do_apply(*args, **kwargs)

################################################################################

class Reactor(object):
    def __init__(self):
        self._orm = ORM.detect(self)

    ############################################################################

    graph  = property(lambda self: self._orm.get('graph',  None))
    mongo  = property(lambda self: self._orm.get('mongo',  None))
    linked = property(lambda self: self._orm.get('linked', None))

#*******************************************************************************

Reactor = Reactor()

