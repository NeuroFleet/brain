#!/usr/bin/python

from gestalt.hub.abstract import *

################################################################################

class Authority(ApplicationSession):
    @inlineCallbacks
    def onJoin(self, details):
       print("Authority.onJoin({})".format(details))
       try:
           yield self.register(self.authorize, 'authoritize')
           print("Authority: authorizer registered")
       except Exception as e:
           print("Authority: failed to register authorizer procedure ({})".format(e))

    def authorize(self, session, uri, action):
       print("Authority.authorize({}, {}, {})".format(session, uri, action))
       return True

