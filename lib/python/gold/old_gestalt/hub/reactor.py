#!/usr/bin/python

from gestalt.hub.abstract import *

################################################################################

class Cortex(Registry):
    prefix  = 'lobes'

    #***************************************************************************

    @pubsub('lymbic.clock.tick')
    def heartbeat(self, counter):
        self.log.info("published to 'lymbic.clock.tick' with counter {counter}", counter=counter)

    @pubsub('lymbic.pneumo.respire')
    def pneumo_respire(self):
        self.log.info("published to 'lymbic.pneumo.respire'.")

    @pubsub('lymbic.pneumo.expire')
    def pneumo_expire(self):
        self.log.info("published to 'lymbic.pneumo.expire'.")

    #***************************************************************************

    @socket_rpc('add2')
    def add2(self, x, y):
        self.log.info("add2() called with {x} and {y}", x=x, y=y)

        return x + y

################################################################################

class Domain(Registry):
    prefix = 'domains'

    #***************************************************************************

    @pubsub('lymbic.clock.tick')
    def heartbeat(self, counter):
        self.log.info("published to 'lymbic.clock.tick' with counter {counter}", counter=counter)

    @pubsub('lymbic.pneumo.respire')
    def pneumo_respire(self):
        self.log.info("published to 'lymbic.pneumo.respire'.")

    @pubsub('lymbic.pneumo.expire')
    def pneumo_expire(self):
        self.log.info("published to 'lymbic.pneumo.expire'.")

    #***************************************************************************

    @socket_rpc('add2')
    def add2(self, x, y):
        self.log.info("add2() called with {x} and {y}", x=x, y=y)

        return x + y

################################################################################

class Lobby(ApplicationSession):
    namespace = 'uchikoma'

    log = Logger()

    #***************************************************************************

    def rpath(self, *parts): return '.'.join([self.namespace]+[x for x in parts])

    #***************************************************************************

    @inlineCallbacks
    def onJoin(self, details):
        targets = [
            Cortex(self, 'leftex'),
            Cortex(self, 'rightex'),
        ]

        targets = []

        #for key in ('business', 'internet'):
        #    targets += [Domain(self, key)]

        #***********************************************************************

        for reg in targets:
            for key in dir(reg):
                callback = getattr(reg, key, None)

                if callable(callback):
                    for channel in getattr(callback, '_pubsub', []):
                        yield self.subscribe(callback, channel)

                        self.log.info("subscribed to topic '%s'" % channel)

                    for address in getattr(callback, '_rpc', []):
                        alias = reg.rpath(address)

                        yield self.register(callback, alias)

                        self.log.info("procedure %s() registered" % alias)

        #***********************************************************************

        counter = 0
        pneumo  = True

        while True:
            yield self.publish('lymbic.clock.cardio', counter)

            if counter % 10:
                if pneumo:
                    yield self.publish('lymbic.pneumo.respire')
                else:
                    yield self.publish('lymbic.pneumo.expire')

                pneumo = not pneumo

            counter += 1

            yield sleep(0.8)

