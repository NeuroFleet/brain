from uchikoma.core.implants import *

################################################################################

class Nerve(Implant):
    def begin(self):
        self.counter = 0

    #***************************************************************************

    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    def process(self):
        counter += 1

    def after(self):
        yield sleep(1)

