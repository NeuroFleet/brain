from uchikoma.core.implants import *

################################################################################

class Organ(Implant):
    def prepare(self, details):
        print("upon connect")

        self.trigger('on_connect')

        print("session joined")

        self.trigger('on_joined')

        print("session ready")

        self.trigger('on_ready')

        print("session running")

    ############################################################################

    def on_connect(self):
        @self.invoke('com.myapp.add2')
        def add_func(x, y):
            return x + y

    #***************************************************************************

    def on_joined(self):
        @self.invoke('com.myapp.add2', 2, 3)
        def add_result(method, resp):
            print("call error: {0}".format(e))

    #***************************************************************************

    def on_ready(self):
        @self.topic('com.myapp.oncounter')
        def oncounter(count):
            print("event received: {0}", count)

    ############################################################################

    def begin(self):
        self.counter = 0

    #***************************************************************************

    def finish(self):
        pass

    ############################################################################

    def before(self):
        self.publish(u'com.myapp.oncounter', self.counter)

    #***************************************************************************

    def process(self):
        counter += 1

    #***************************************************************************

    def after(self):
        yield sleep(1)

