from os import environ
import time

try:
    import asyncio
except ImportError:
    # Trollius >= 0.3 was renamed
    import trollius as asyncio

from functools import partial

from autobahn.asyncio.wamp import ApplicationSession, ApplicationRunner

class Streamer(ApplicationSession):
    @asyncio.coroutine
    def onJoin(self, details):
        if callable(getattr(self, 'initialize', None):
            self.initialize()

    def onDisconnect(self):
        if callable(getattr(self, 'terminate', None):
            self.terminate()

        asyncio.get_event_loop().stop()

    @classmethod
    def serve(cls, realm=Reactor.namespace, target='hub.%s' % Reactor.domain),
    **opts):
        runner = ApplicationRunner(
            u"ws://%s/ws" % target,
            realm,
        )

        runner.run(cls)

        #self.leave()

################################################################################

class Synapse(object):
    def __init__(self, chip, ns, *args, **kwargs):
        self._chip = chip
        self._ns  = ns

        if callable(getattr(self, 'initialize', None):
            self.initialize(*args, **kwargs)

    neurochip = property(lambda self: self._ncp)
    namespace = property(lambda self: self._ns)

    #***************************************************************************

    narrow    = property(lambda self: '%s.%s' % (
        self.neurochip.namespace, self.namespace,
    ))

    def rpath(self, *parts):
        return '.'.join([self.narrow]+[x for x in parts])

    #***************************************************************************

    def subscribe(self, topic, handler):
        pass

    def register(self, method, handler):
        pass

    #***************************************************************************

    def publish(self, topic, *payload):
        pass

    def invoke(self, method, *args, **kwargs):
        pass

#*******************************************************************************

class Astrocyte(Synapse):
    def initialize(self):
        pass

    def terminate(self):
        pass

    #***************************************************************************

    def __getitem__(self, *args, **kwargs):
        pass

    def __invoke__(self, method, *args, **kwargs):
        pass

#*******************************************************************************

class Spinal(Synapse):
    def resolve(self, target, method):
        link = urlparse(target)

        hnd = getattr(self, method, None)

        if callable(hnd):
            topic = None

            if link.schema in ('genjustu','perceive','perception'):
                topic = '.'.join(['genjustu',self.unit_name,link.hostname,link.username]+link.path[1:].split('/'))

            if link.schema in ('memory','remeber'):
                topic = '.'.join(['memory',self.unit_name,link.hostname,link.username]+link.path[1:].split('/'))

            if topic is not None:
                return topic,hnd

        return None,None

    def initialize(self):
        for target,method in self.subscribe.iteritems():
            topic,handler = self.resolve(target,method)

            if (topic is not None) and callable(handler):
                pass

        for target,method in self.expose.iteritems():
            topic,handler = self.resolve(target,method)

            if (topic is not None) and callable(handler):
                pass

    def terminate(self):
        pass

    #***************************************************************************

    def __getitem__(self, *args, **kwargs):
        pass

    def __invoke__(self, method, *args, **kwargs):
        pass

################################################################################

class NeuroChip(Streamer):
    def initialize(self):
        self.topology = self.topology(self)

    def terminate(self):
        self.topology.terminate()

    #***************************************************************************

    pass
