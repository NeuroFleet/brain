Zen SHell (ZSH) :
=================

The default configuration used in ZSH is :

```zsh
CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
export UPDATE_ZSH_DAYS=13
COMPLETION_WAITING_DOTS="true"
```

Using [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh), we used the "xiong-chiamiov-plus" as default theme and enabled the following plugins :

* ant
* cloudapp
* compleat
* command-not-found
* coffee
* django
* docker
* encode64
* gem
* gitfast
* git-extras
* git-flow
* git-hubflow
* github
* grails
* jira
* heroku
* history
* history-substring-search
* jira
* knife
* knife_ssh
* last-working-dir
* lol
* mvn
* nanoc
* node
* npm
* nyan
* phing
* pip
* postgres
* profiles
* pyenv
* python
* rails
* rake
* rand-quote
* redis-cli
* ruby
* rvm
* screen
* supervisor
* torrent
* urltools
* vagrant
* virtualenv
* virtualenvwrapper
* web-search
* wd
