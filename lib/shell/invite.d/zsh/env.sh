#!/bin/zsh

export UPDATE_ZSH_DAYS=13

CASE_SENSITIVE="false"
DISABLE_AUTO_UPDATE="true"
COMPLETION_WAITING_DOTS="true"

DEBIAN_PREVENT_KEYBOARD_CHANGES=yes
# DISABLE_UNTRACKED_FILES_DIRTY="true"

export DEBIAN_PREVENT_KEYBOARD_CHANGES=yes

#*******************************************************************************

# Set up the prompt
autoload -Uz promptinit
autoload -Uz add-zsh-hook
promptinit
#prompt adam1
setopt histignorealldups sharehistory

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.config/zsh_history

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

#*******************************************************************************

current_branch () {
  git branch 2>&1 | grep -e fatal -e error -v | grep -e "^* " | sed -e 's/^\* //g'
}

#*******************************************************************************

export RONIN_ROOT=/ron
export RONIN_BOOT=$RONIN_ROOT/boot

ZSH_PLUGINs=(debian profiles wd)
#ZSH_PLUGINs=(debian last-working-dir profiles wd)
#ZSH_PLUGINs+=(ssh-agent gpg-agent)
#ZSH_PLUGINs+=(pass phing screen)
ZSH_PLUGINs+=(catimg torrent)
ZSH_PLUGINs+=(nyan rand-quote)

ZSH_PLUGINs+=(encode64 jsontools urltools)

ZSH=true

source $RONIN_BOOT/shell/env.sh

export PATH=$RONIN_PATH":"$PATH

#*******************************************************************************

if [[ $USER == 'root' ]] ; then
  ZSH_THEME="xiong-chiamiov-plus"
else
  ZSH_THEME="bira"
fi

if [[ -f $HOME/.bushido/env.zsh ]] ; then
  source $HOME/.bushido/env.zsh
else
  echo Warning : Missing Bushido configuration.
fi

#*******************************************************************************

#plugins=(command-not-found common-aliases compleat dirhistory history per-directory-history)
plugins=(command-not-found common-aliases compleat history)

for ext in $ZSH_PLUGINs ; do
  plugins+=($ext)
done

unset ZSH, ZSH_PLUGINs

#*******************************************************************************

export ANTIGEN_PATH=$RONIN_BOOT/shell/zsh/antigen

source $ANTIGEN_PATH/antigen.zsh

antigen use oh-my-zsh

antigen bundle lein
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle Tarrasch/zsh-autoenv

# Bundles from the default repo (robbyrussell's oh-my-zsh).
for T4 in $plugins ; do
  antigen bundle $T4
done

# Load the theme.
antigen theme $ZSH_THEME

# Tell antigen that you're done.
antigen apply

#*******************************************************************************

export GIT_EDITOR=vim
export VISUAL=vim
export EDITOR=vim

#*******************************************************************************

#function printc () {
#    print "printc" $1
#}

#add-zsh-hook preexec printc
