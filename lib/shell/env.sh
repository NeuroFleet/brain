export NOH_ROOT="/ron/usr"
export RONIN_ROOT="/ron"

export RONIN_BOOT=$RONIN_ROOT'/boot'
export RONIN_PATH=$RONIN_ROOT'/sbin'

export RONIN_HOME_ROOT=/root
export RONIN_HOME_DIR=/home

export RONIN_LANG_SUB=lang
export RONIN_RECIPE=kyogen

#*******************************************************************************************************************************

export RONIN_OS_DIR=$NOH_ROOT/os
export RONIN_CHARMS_DIR=$NOH_ROOT/charms

#*******************************************************************************************************************************

export RONIN_LANG_DIR=$RONIN_ROOT/$RONIN_LANG_SUB
export RONIN_COOKBOOK=$NOH_ROOT/$RONIN_RECIPE

#*******************************************************************************************************************************

if [[ -f $NOH_ROOT/env.sh ]] ; then
    source $NOH_ROOT/env.sh
fi

#*******************************************************************************************************************************

export RONIN_LANG_DIR=$RONIN_ROOT/$RONIN_LANG_SUB
export RONIN_COOKBOOK=$NOH_ROOT/$RONIN_RECIPE

################################################################################################################################

source $RONIN_BOOT/shell/specs.sh

ensure_os_specs

#*******************************************************************************

if [[ -d $RONIN_SPECS/tools ]] ; then
    RONIN_PATH=$RONIN_PATH":"$RONIN_SPECS/tools
fi

################################################################################################################################

if [[ -d $RONIN_SDK_DIR ]] ; then
    for key in `ls $RONIN_SDK_DIR` ; do
        export SDK_NAME=$key
        export SDK_PATH=$RONIN_SDK_DIR/$key

        if [[ -e $SDK_PATH/env.sh ]] ; then
            source $SDK_PATH/env.sh
        fi

        if [[ -d $SDK_PATH/sbin ]] ; then
            RONIN_PATH=$RONIN_PATH":"$SDK_PATH/sbin
        fi
    done
fi

#*******************************************************************************************************************************

for key in `ls $RONIN_LANG_DIR` ; do
    export LANG_NAME=$key
    export LANG_PATH=$RONIN_LANG_DIR/$LANG_NAME
    export LANG_INCLUDES=$LANG_PATH/dist

    for nrw in `ls $RONIN_CHARMS_DIR` ; do
      for rpath in $RONIN_LANG_SUB ; do
        TARGET_FOLDER=$RONIN_CHARMS_DIR/$nrw/$rpath/$LANG_NAME

        if [[ -d $TARGET_FOLDER ]] ; then
          export LANG_INCLUDES=$LANG_INCLUDES" "$TARGET_FOLDER
        fi
      done
    done

    echo "  -> supporting '$LANG_NAME' at : $LANG_INCLUDES"

    if [[ -e $LANG_PATH/env.sh ]] ; then
        source $LANG_PATH/env.sh
    fi

    if [[ -d $LANG_PATH/tools ]] ; then
        RONIN_PATH=$RONIN_PATH":"$LANG_PATH/tools
    fi
    unset LANG_NAME LANG_PATH LANG_INCLUDES

done

################################################################################################################################

if [[ -e $RONIN_COOKBOOK ]] ; then
    for key in `ls $RONIN_COOKBOOK` ; do
        export RECIPE_NAME=$key
        export RECIPE_PATH=$RONIN_SDK_DIR/$RECIPE_NAME

        if [[ -e $RECIPE_PATH/env.sh ]] ; then
            source $RECIPE_PATH/env.sh
        fi

        if [[ -d $RECIPE_PATH/sbin ]] ; then
            RONIN_PATH=$RONIN_PATH":"$RECIPE_PATH/sbin
        fi

        unset RECIPE_NAME RECIPE_PATH
    done
fi

#*******************************************************************************************************************************

for key in `ls $RONIN_CHARMS_DIR` ; do
    export CHARM_NAME=$key
    export CHARM_PATH=$RONIN_SDK_DIR/$CHARM_NAME

    if [[ -d $CHARM_PATH ]] ; then
        if [[ -f $CHARM_PATH/env.sh ]] ; then
            source $CHARM_PATH/env.sh
        fi

        if [[ -d $CHARM_PATH/sbin ]] ; then
            RONIN_PATH=$RONIN_PATH":"$CHARM_PATH/sbin
        fi
    fi

    unset CHARM_NAME CHARM_PATH
done

#*******************************************************************************************************************************

if [ $RONIN_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",nano"
    export CYG_PKGs=$CYG_PKGs",bash,dash,fish,mksh,zsh"
    export CYG_PKGs=$CYG_PKGs",tree,htop,nmap"
fi

if [ $RONIN_OS=="macosx" ] ; then
    export BREW_PKGs=$BREW_PKGs" mksh"
fi

if [ $RONIN_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" mksh zsh zsh-lovers"
    export APT_PKGs=$APT_PKGs" tree htop fping nmap traceroute"
fi

export NPM_PKGs="$NPM_PKGs nsh"

source $RONIN_BOOT/shell/aliases.sh

#*******************************************************************************************************************************

# shortcuts
#alias folder="mkdir -p "

alias ll="ls -lShra "
alias lt="ls -lthra "

alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

#*******************************************************************************

alias dfh="df -h "

diskusage () {
  du -hd1 $* | sort -h
}

#*******************************************************************************

# Shell common helpers

alias random='od -vAn -N4 -tu4 < /dev/urandom'
alias ssh-clear='ssh-keygen -f $HOME/.ssh/known_hosts -R '
alias pwdgen="< /dev/urandom tr -dc 'A-Za-z0-9\\?!=-_' | head -c13"

# Shell listing enhancement

alias lh='ll -h'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias lsize='ls -l $1 | wc -l'

# POSIX enhancement

alias reboot="sudo shutdown -r now"
alias :q="exit"

# Supervisor's shortcuts

alias superctl='supervisorctl'
alias superstart='supervisorctl start'
alias superrestart='supervisorctl restart'
alias superstop='supervisorctl stop'
alias supertail='supervisorctl tail -f'

#*******************************************************************************

alias EDITOR="nano "

#*******************************************************************************************************************************

export RONIN_PATH

source /etc/environment

cat $NOH_ROOT/motd.post
echo

#*******************************************************************************************************************************

if [[ -f $HOME/.shelter/env.sh ]] ; then
     source $HOME/.shelter/env.sh
fi

export RONIN_ENV_GEN=$RONIN_BOOT/shell/env.gen

if [[ $"USER" == "root" ]] ; then
  set >$RONIN_ENV_GEN
fi
