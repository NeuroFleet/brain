declare_os_specs () {
  export HELLO=WORLD
}

require_os_specs () {
  export TARGET_KEY=$1
  export TARGET_DIR=$GHOST_FOLDER/boot/os/$TARGET_KEY

  if [[ ! -d $TARGET_DIR ]] ; then
      git clone https://bitbucket.org/it-issal/$TARGET_KEY.git $TARGET_DIR
  fi

  if [[ ! -d $TARGET_DIR ]] ; then
      echo "Could'nt find the specs for your Operating System '"$GHOST_SYSTEM"' at : "$GHOST_SYSDIR

      echo "Sorry, the Shelter can't bootup. Exiting ..."

      exit
  fi
}

ensure_os_specs () {
  if [[ -f /etc/os-release ]] ; then
      source /etc/os-release

      for KEY in ID ID_LIKE NAME PRETTY_NAME VERSION VERSION_ID HOME_URL SUPPORT_URL BUG_REPORT_URL ANSI_COLOR ; do
        VALUE=`eval 'echo \$'$KEY`

        if [[ "$VALUE" != "" ]] ; then
          eval 'export GHOST_SYSTEM_'$KEY'="'$VALUE'"'
          eval 'export DISTRIB_'$KEY'="'$VALUE'"'

          #eval "export GHOST_SYSTEM_"$KEY"=\$"$KEY
          #eval "export DISTRIB_"$KEY"=\$"$KEY

          unset $KEY
        fi
      done
  fi

  #*******************************************************************************************************************************

  case $DISTRIB_ID in
      raspbian)
          export GHOST_SYSTEM="raspbian"
          ;;
      debian|Debian)
          export GHOST_SYSTEM="debian"
          ;;
      ubuntu|Ubuntu)
          export GHOST_SYSTEM="ubuntu"
          ;;
      *)
          case $OSTYPE in
              cygwin)
                  export GHOST_SYSTEM="windows"

                  export RONIN_HOME_ROOT=$HOME
                  export RONIN_HOME_DIR=$HOMEDRIVE"\Users"
                  ;;
              *)
                  export GHOST_SYSTEM="linux"

                  if [[ -f /usr/bin/yum ]] ; then
                      export GHOST_SYSTEM="redhat"
                  fi

                  if [[ -d /Users ]] ; then
                      export GHOST_SYSTEM="macosx"

                      export RONIN_HOME_ROOT=/var/root
                      export RONIN_HOME_DIR=/Users
                  fi
                  ;;
          esac
      ;;
  esac

  #*******************************************************************************************************************************

  export GHOST_SYSDIR=$GHOST_FOLDER/boot/os/$GHOST_SYSTEM

  require_os_specs $GHOST_SYSTEM

  #*******************************************************************************

  export OS_NAME=$GHOST_SYSTEM
  export OS_PATH=$GHOST_SYSDIR

  for key in ID NAME VERSION ANSI_COLOR ; do
    if [[ "`eval 'echo \$GHOST_SYSTEM_'$key`" != "" ]] ; then
      eval "export OS_$key=\$GHOST_SYSTEM_$key"
    fi
  done

  source $GHOST_SYSDIR/env.sh

  unset OS_NAME OS_PATH

  for key in ID NAME VERSION ANSI_COLOR ; do
    if [[ "`eval 'echo \$OS_'$key`" != "" ]] ; then
      eval "unset OS_"$key
    fi
  done
}

