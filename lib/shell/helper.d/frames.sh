#!/bin/bash

runner () {
    if [[ -f /app/newrelic.ini ]] ; then
        newrelic-admin run-program $*
    else
        exec $*
    fi
}

symlink () {
    if [[ ! -d $2 ]] ; then
        if [[ ! -f $2 ]] ; then
            ln -sf $1 $2
        fi
    fi
}

#*****************************************************************************************************

neurochip_infos () {
    echo
    echo "Ghost detected :"
    neurochip_infos_ghost
    echo
    echo "Using platform :"
    neurochip_infos_platform
    echo
    echo "Extensions supported :"
    echo "    Modules   : "$GHOST_ABILITY
    echo "    Profiles  : "$GHOST_PROFILE
    echo
}
neurochip_details () {
    echo
    echo "Ghost detected :"
    neurochip_infos_ghost
    echo
    echo "Using platform :"
    neurochip_infos_platform
    echo
    echo "Connected via :"
    neurochip_infos_network
    echo
    echo "Extensions supported :"
    echo "    Systems   : "$GHOST_SYSTEMS
    echo "    Languages : "$GHOST_TONGUES
    echo "    Modules   : "$GHOST_ABILITY
    echo "    Profiles  : "$GHOST_PROFILE
    echo
    echo "Deployment infos : "
    neurochip_infos_deploy
    echo
}

neurochip_infos_ghost () {
    echo "    Domain : "$GHOST_DOMAIN
    echo "    Narrow : "$GHOST_NARROW

    if [[ "x$GHOST_PERSON" != "x" ]] ; then
        echo "    Person : "$GHOST_PERSON
    fi
}
neurochip_infos_platform () {
    echo "    Hostname : "`hostname -f`
    echo "    System   : "$GHOST_SYSTEM
    echo "    Module   : "$GHOST_MODULE
}
neurochip_infos_network () {
    echo "    Teredo   : "$TEREDO_ADDR
}
neurochip_infos_deploy () {
    echo "    -> Java    : "$CLASSPATH
    echo "    -> Python  : "$PYTHONPATH
    echo "    -> Node.js : "$NODE_PATH
}

################################################################################

ensure_fs_core () {
    if [[ ! -d $GHOST_SPECS ]] ; then
        git clone https://bitbucket.org/$GHOST_NARROW/$GHOST_PERSON.git $GHOST_SPECS --recursive
    fi

    #*******************************************************************************

}

ensure_fs_exts () {
    for ext in `echo $GHOST_REACTOR $GHOST_FRAMES` ; do
        if [[ ! -d $GHOST_LIB/python/dist/$ext ]] ; then
            git clone https://bitbucket.org/NeuroMATE/$ext.git $GHOST_LIB/python/dist/$ext
        fi
    done

    #*******************************************************************************

    for app in $GHOST_AGENTS ; do
        if [[ ! -d $GHOST_LIB/python/dist/$GHOST_NARROW/$app ]] ; then
            git clone https://bitbucket.org/$GHOST_NARROW/$app.git $GHOST_LIB/python/dist/$GHOST_NARROW/$app
        fi
    done
}

